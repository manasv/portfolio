/* 
ProjectDetail.jsx
Manuel Sanchez
301275701
*/

import React from "react";
import { useParams } from "react-router-dom";
import magusImage from "../assets/magus.png";
import updetoImage from "../assets/updeto.webp";
import palmateImage from "../assets/palmate.png";

function ProjectDetail() {
  const { id } = useParams();
  const project = projectData[id];

  if (!project) {
    return (
      <div className="min-h-screen bg-gray-100 dark:bg-gray-900 p-4 flex items-center justify-center text-gray-800 dark:text-gray-100">
        Project not found
      </div>
    );
  }

  return (
    <div className="w-full min-h-screen bg-gray-100 dark:bg-gray-900">
      <div className="max-w-screen-xl mx-auto p-8">
        <h1 className="text-5xl font-bold mb-8 text-center text-gray-800 dark:text-gray-100">
          {project.title}
        </h1>
        <img
          src={project.img}
          alt={project.title}
          className="h-96 mx-auto object-cover mb-8"
        />
        <div className="mx-auto">
          <p className="text-lg text-gray-600 dark:text-gray-300 mb-8">
            {project.description}
          </p>
          <div dangerouslySetInnerHTML={{ __html: project.details }} />
        </div>
      </div>
    </div>
  );
}

export default ProjectDetail;

const projectData = {
  1: {
    id: 1,
    title: "MaGus Project",
    description: "Standardizing the process of building iOS apps.",
    img: magusImage,
    details: `
    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">Description</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">At our previous company, we were driven by a clear mission: to standardize the process of building iOS apps. This goal was rooted in the need for consistency, efficiency, and quality across all our iOS projects. To achieve this, we developed a powerful tool called MaGus.</p>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">MaGus revolutionized our development workflow by enabling us to generate a complete Xcode project, adhering strictly to our predefined structure and best practices. This tool not only streamlined the initial setup of new projects but also ensured that every app we created followed the same high standards.</p>
    
    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">My Role</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">I played a pivotal role in the development and implementation of MaGus. My responsibilities included defining the project structure and best practices that MaGus would enforce, coding the tool to automate the project generation process, and integrating it with our existing development environment. I also collaborated with the team to gather requirements, conduct testing, and refine the tool based on feedback.</p>
    
    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">Outcome</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">The introduction of MaGus brought numerous benefits to our development process. It reduced onboarding time for new developers, minimized errors, and made it easier to maintain and scale our codebases. MaGus became an integral part of our workflow, embodying our commitment to excellence and innovation in iOS app development. The tool's ability to generate a complete Xcode project with our predefined structure significantly increased our efficiency and ensured a high level of consistency across all our iOS projects.</p>
  `,
  },
  2: {
    id: 2,
    title: "Updeto Project",
    description: "Ensure your app is always up to date.",
    img: updetoImage,
    details: `
    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">Description</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">The moment when you need to ensure your users are running the latest version of your iOS app is crucial for maintaining a seamless user experience and delivering the latest features and security updates. To address this need, we developed Updeto, a tool designed to quickly and efficiently check if users are running the most current version of your app.</p>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">Updeto provides a straightforward and effective solution for version control, enabling developers and support teams to verify app versions in real-time. This tool ensures that users have access to the latest updates, reducing potential issues caused by outdated software.</p>

    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">My Role</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">In the development of Updeto, my role was instrumental in designing the tool's core functionality and user interface. I was responsible for implementing the version-checking algorithms and integrating them with our backend systems. Additionally, I worked on optimizing the tool for performance and accuracy, ensuring that version checks are conducted swiftly without compromising reliability.</p>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">I also collaborated closely with the UX/UI team to create an intuitive interface that makes it easy for developers and support teams to use Updeto effectively. This involved user testing and iterative design improvements based on feedback.</p>

    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">Outcome</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">The introduction of Updeto brought significant benefits to our development and support processes. It streamlined the version-checking procedure, allowing our team to ensure users are always on the latest version with minimal effort. This, in turn, improved the overall user experience by ensuring that all users had access to the latest features and security updates.</p>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">Updeto's efficient version control helped reduce support tickets related to outdated app versions and decreased the time spent troubleshooting version-related issues. The tool became a vital part of our maintenance workflow, highlighting our commitment to providing a seamless and secure user experience.</p>
  `,
  },
  3: {
    id: 3,
    title: "PalMate Project",
    description: "Pokedex-like app for the indie game PalWorld.",
    img: palmateImage,
    details: `
    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">Description</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">PalMate is a Pokedex-like app designed for the indie game PalWorld. This app provides players with a comprehensive tool to track every Pal in the game, offering detailed information on each Pal's location, number of catches, and possible drops. The goal of PalMate is to enhance the gaming experience by giving players a reliable resource to manage and optimize their Pal collection and gameplay strategies.</p>

    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">My Role</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">As the main developer of PalMate, I was responsible for designing and implementing all the core features of the app. My duties included creating the database to store Pal information, developing the user interface to display this information intuitively, and integrating the app with PalWorld's game data. I also worked on optimizing the app for performance and usability, ensuring that players could easily access and interact with the information they needed.</p>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">I collaborated closely with the game developers to ensure that PalMate accurately reflected in-game data and provided real-time updates. Additionally, I conducted user testing to gather feedback and make iterative improvements to the app's functionality and design.</p>

    <h2 class="text-3xl font-semibold mb-4 text-gray-800 dark:text-gray-100">Outcome</h2>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">The launch of PalMate significantly enhanced the gaming experience for PalWorld players. The app's comprehensive tracking and management features allowed players to keep detailed records of their Pal collections, improving their gameplay strategies and overall enjoyment of the game.</p>
    <p class="text-lg mb-4 text-gray-600 dark:text-gray-300">PalMate received positive feedback from the player community for its intuitive interface and reliable performance. The app's ability to provide real-time updates and accurate information helped players make informed decisions during their gameplay, making it an invaluable companion for PalWorld enthusiasts.</p>
  `,
  },
};
