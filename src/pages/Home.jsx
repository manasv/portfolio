/* 
Home.jsx
Manuel Sanchez
301275701
*/

import React from 'react';
import { Link } from 'react-router-dom';

function Home() {
  return (
    <div className="w-full min-h-screen bg-gray-100 dark:bg-gray-900 flex justify-center items-center">
      <div className="max-w-screen-xl mx-auto p-4 text-center">
        <h1 className="text-6xl font-bold mb-6 text-gray-800 dark:text-gray-100">Welcome to My iOS Development Portfolio</h1>
        <p className="text-xl mb-6 text-gray-600 dark:text-gray-300">Crafting elegant and efficient mobile solutions for iOS.</p>
        <Link to="/about" className="bg-green-700 text-white text-lg font-semibold px-6 py-3 rounded hover:bg-green-900">Learn more about me</Link>
      </div>
    </div>
  );
}

export default Home;
