/* 
Services.jsx
Manuel Sanchez
301275701
*/

import React from 'react';

function Services() {
  return (
    <div className="w-full min-h-screen bg-gray-100 dark:bg-gray-900">
      <div className="max-w-screen-xl mx-auto p-8">
        <h1 className="text-5xl font-bold mb-8 text-center text-gray-800 dark:text-gray-100">Services</h1>
        <p className="text-xl mb-8 text-gray-600 dark:text-gray-300 text-center">I offer a variety of services to help you create and maintain high-quality iOS applications. My services include:</p>
        <ul className="list-disc list-inside text-lg text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
          <li className="mb-4">
            <h2 className="text-2xl font-semibold mb-2 text-gray-800 dark:text-gray-100">Custom iOS App Development</h2>
            <p>From concept to deployment, I provide end-to-end iOS app development services. Whether you need a new app built from scratch or want to enhance an existing one, I use the latest technologies and best practices to deliver robust, scalable, and user-friendly applications tailored to your needs.</p>
          </li>
          <li className="mb-4">
            <h2 className="text-2xl font-semibold mb-2 text-gray-800 dark:text-gray-100">App Store Deployment</h2>
            <p>Navigating the App Store submission process can be challenging. I offer comprehensive support for deploying your app, including preparing metadata, creating promotional materials, and ensuring compliance with Apple's guidelines. I handle the entire submission process to get your app approved and published smoothly.</p>
          </li>
          <li className="mb-4">
            <h2 className="text-2xl font-semibold mb-2 text-gray-800 dark:text-gray-100">App Maintenance and Support</h2>
            <p>Keeping your app up-to-date is crucial for maintaining user satisfaction and security. I provide ongoing maintenance and support services, including bug fixes, performance improvements, and updates to ensure compatibility with the latest iOS versions and devices. My support services are designed to keep your app running smoothly and efficiently.</p>
          </li>
          <li className="mb-4">
            <h2 className="text-2xl font-semibold mb-2 text-gray-800 dark:text-gray-100">Integration with Third-Party APIs</h2>
            <p>Modern apps often rely on third-party services to enhance functionality. I have extensive experience integrating apps with various APIs, including payment gateways, social media platforms, and cloud services. I ensure seamless integration to expand your app's capabilities and provide a richer user experience.</p>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Services;
