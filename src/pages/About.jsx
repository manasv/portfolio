/* 
About.jsx
Manuel Sanchez
301275701
*/

import React from 'react';
import profilePicture from '../assets/profile_picture.webp';
import resumePath from '../assets/manuel-sanchez.pdf';

function About() {
  return (
    <div className="w-full min-h-screen bg-gray-100 dark:bg-gray-900">
      <div className="max-w-screen-xl mx-auto p-8">
        <h1 className="text-5xl font-bold mb-8 text-center text-gray-800 dark:text-gray-100">About Me</h1>
        <div className="flex flex-col items-center">
          <img src={profilePicture} alt="About me" className="w-80 h-80 rounded-full mb-4" />
          <p className="text-2xl mb-4 text-gray-600 dark:text-gray-300">Manuel Sanchez</p>
          <p className="text-center max-w-2xl text-lg text-gray-600 dark:text-gray-300 mb-8">I am a passionate iOS developer with over 5 years of experience in creating innovative and user-friendly mobile applications. My expertise lies in Swift and Objective-C, and I have a strong background in UX/UI design. I enjoy working on challenging projects and continuously learning new technologies to improve my skills.</p>
          <a href={resumePath} download className="mt-4 bg-green-700 text-white text-lg font-semibold px-4 py-2 rounded hover:bg-green-900">Download Resume</a>
        </div>
      </div>
    </div>
  );
}

export default About;
