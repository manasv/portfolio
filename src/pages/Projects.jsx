/* 
Projects.jsx
Manuel Sanchez
301275701
*/

import React from 'react';
import { Link } from 'react-router-dom';
import magusImage from '../assets/magus.png';
import updetoImage from "../assets/updeto.webp";
import palmateImage from "../assets/palmate.png";

function Projects() {
  const projectList = [
    { id: 1, title: 'MaGus', description: 'We had a mision, standarize how we build iOS apps at our past company, and we did it with MaGus, being able to generate a full Xcode project with our structure.', img: magusImage },
    { id: 2, title: 'Updeto', description: 'The moment when you need to ensure your users are running the latest version of your iOS App is here, with updeto you can check that super quick.', img: updetoImage },
    { id: 3, title: 'PalMate', description: 'Pokedex-like app for the indie game PalWorld, gives the opportunity to track every pal, with their location, number of catches and also possible drops.', img: palmateImage }
  ];

  return (
    <div className="w-full min-h-screen bg-gray-100 dark:bg-gray-900">
      <div className="max-w-screen-xl mx-auto p-8">
        <h1 className="text-5xl font-bold mb-8 text-center text-gray-800 dark:text-gray-100">Projects</h1>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
          {projectList.map(project => (
            <div key={project.id} className="bg-white dark:bg-gray-800 p-6 shadow rounded">
              <img src={project.img} alt={project.title} className="w-full h-48 object-cover mb-4" />
              <h2 className="text-2xl font-bold mb-2 text-gray-800 dark:text-gray-100">{project.title}</h2>
              <p className="text-lg text-gray-600 dark:text-gray-300">{project.description}</p>
              <Link to={`/projects/${project.id}`} className="text-green-700 dark:text-green-500 hover:underline mt-4 block">Read more</Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Projects;
