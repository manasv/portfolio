/* 
CustomLogo.jsx
Manuel Sanchez
301275701
*/

import React from 'react';
import logo from '../assets/logo.svg'; 

function CustomLogo() {
  return (
    <div className="logo">
      <img src={logo} alt="Manuel Sanchez" height={50} width={50}/>
    </div>
  );
}

export default CustomLogo;