/* 
NavBar.jsx
Manuel Sanchez
301275701
*/

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { FiSun, FiMoon } from 'react-icons/fi';
import CustomLogo from './CustomLogo';

function Navbar() {
  const [darkMode, setDarkMode] = useState(false);

  useEffect(() => {
    const currentTheme = localStorage.getItem('theme');
    if (currentTheme === 'dark' || (!currentTheme && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
      document.documentElement.classList.add('dark');
      setDarkMode(true);
    } else {
      document.documentElement.classList.remove('dark');
      setDarkMode(false);
    }
  }, []);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
    if (darkMode) {
      document.documentElement.classList.remove('dark');
      localStorage.setItem('theme', 'light');
    } else {
      document.documentElement.classList.add('dark');
      localStorage.setItem('theme', 'dark');
    }
  };

  return (
    <div className="bg-green-700 dark:bg-gray-800 w-full">
      <nav className="max-w-screen-xl mx-auto p-4 flex justify-between items-center sticky top-0 z-10">
        <CustomLogo />
        <ul className="flex space-x-4 items-center">
          <li><Link to="/" className="text-lg font-medium text-white dark:text-gray-300 hover:text-gray-300 dark:hover:text-white">Home</Link></li>
          <li><Link to="/about" className="text-lg font-medium text-white dark:text-gray-300 hover:text-gray-300 dark:hover:text-white">About Me</Link></li>
          <li><Link to="/projects" className="text-lg font-medium text-white dark:text-gray-300 hover:text-gray-300 dark:hover:text-white">Projects</Link></li>
          <li><Link to="/services" className="text-lg font-medium text-white dark:text-gray-300 hover:text-gray-300 dark:hover:text-white">Services</Link></li>
          <li><Link to="/contact" className="text-lg font-medium text-white dark:text-gray-300 hover:text-gray-300 dark:hover:text-white">Contact Me</Link></li>
          <li>
            <button onClick={toggleDarkMode} className="text-lg font-medium text-white dark:text-gray-300 hover:text-gray-300 dark:hover:text-white p-2 rounded focus:outline-none">
              {darkMode ? <FiSun size={24} /> : <FiMoon size={24} />}
            </button>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Navbar;
